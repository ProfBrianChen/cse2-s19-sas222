////////
//// HW09, ArrayGames.java
//// Generates random arrays and does a variety of manipulations
//@author ssaini
//@version April 15, 2019
///

import java.util.Scanner;

public class ArrayGames {
	public static int[] generate(){ //Generates array of size between 10 and 20 with values between 1 and 20
		int sizeOfArray = (int)(Math.random() * 21 + 10);
		int[] myArray = new int[sizeOfArray];
		for (int i = 0; i < myArray.length; i++){
			myArray[i] = (int)(Math.random() * 21 + 1);
		}
		return myArray;
	}//end of generate method
	
	public static void print(int[] arr){  //Prints all elements of an array in a horizontal list (with commas)
		System.out.print("{" + arr[1]);
		for (int i = 1; i < arr.length; i ++){
			System.out.print(","+ arr[i]);
		}
		System.out.print("}");
	} //end of print method
	
	
	public static int[] insert (int[] arr1, int[] arr2){  //Combines both arrays
		int[] finalArray = new int[arr1.length + arr2.length];
		int randomMember = (int) (Math.random() * arr1.length + 1);
		for (int i = 0; i < randomMember; i ++){
			finalArray[i] = arr1[i];
		}
		for (int i = 0; i < arr2.length; i ++){
			finalArray[i + randomMember] = arr2[i];
		}		
		for (int i = randomMember; i < arr1.length; i ++){
			finalArray[i + arr2.length] = arr1[i];
		}
		return finalArray;
	} //end of insert method
	
	
	public static int[] shorten(int[] arr, int number){//Removes a random element of the array
		int[] ourArray;
		if (number < arr.length){
			ourArray = new int[arr.length - 1];
			for(int i = 0; i < number; i ++){
				ourArray[i] = arr[i];
			}
			for(int i = number + 1; i < ourArray.length; i ++){
				ourArray[i-1] = arr[i];
			}
		}
		else{
			ourArray = arr;
		}
		return ourArray;
	}//end of shorten method
	
	public static void main(String[] args) {  //Main method
		// TODO Auto-generated method stub
		Scanner myScanner;
		String userInput;
    
		System.out.print("Which would you like to run: insert or shorten? ");
		boolean right = true;
		do{
			myScanner = new Scanner(System.in);
			userInput = myScanner.next().toLowerCase();
			if (userInput.equals("insert") || userInput.equals("shorten")){
				right = false;
			}
			else{
				System.out.println();
				System.out.print("Error. Please enter \"insert\" or \"shorten\" here: ");
			}
		}while(right);
		
    //Makes the arrays
		int[] myFirst = generate();		
		int[] mySecond = generate();
		
		
		if (userInput.equals("insert")){ //If user chose Insert
			System.out.print("Input 1: "); 
			print(myFirst);
			System.out.print("\t Input 2: "); 	
			print(mySecond);
			
			System.out.println();		
			System.out.println();
			
			int[] allOfThem = insert(myFirst, mySecond);
			System.out.println("Output:" );
			print(allOfThem);
      System.out.println();
		}
		
		else{                           //If user chose shorten
			int thisOne = (int)(Math.random() * 2 + 1);
			int[] thisArray;
			if (thisOne == 1){
				thisArray = myFirst;
			}
			else{
				thisArray = mySecond;
			}
			int randomNumber = (int) (Math.random() * 40 + 1);
			
			
			System.out.print("Input 1: "); 
			print(thisArray);
			System.out.print("\t Input 2: " + randomNumber); 	
			
			System.out.println();		
			System.out.println();	
			
			int[] cutArray = shorten(thisArray, randomNumber);
			
			print(cutArray);
      System.out.println();
		}	
	} //end of main method

} //end of class
