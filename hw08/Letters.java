////////
//// HW08, Letters.java
//// Divides an array of letters into 2 separate arrays (A-M and N-Z)
//@author ssaini
//@version April 8, 2019
///

public class Letters {
	public static char[] getAtoM (char[] a){ //Returns array of letters that are between A and M
		int lengthOfArray = a.length;
		char[] fromAtoM = new char[lengthOfArray];
		
		for(int i = 0; i < lengthOfArray; i++){
			char currentChar = Character.toUpperCase(a[i]);
			int charValue = Character.getNumericValue(currentChar);	
		
			if (charValue >= 10 && charValue <= 22){
				fromAtoM[i] = a[i];
			}
		}
		return fromAtoM;
	} //end of method to return A to M

	public static char[] getNtoZ (char[] a){ //Returns an array of letters that are between N and Z
		int lengthOfArray = a.length;
		char[] fromNtoZ = new char[lengthOfArray];
		
		for(int i = 0; i < lengthOfArray; i++){
			char currentChar = Character.toUpperCase(a[i]);
			int charValue = Character.getNumericValue(currentChar);	
		
			if (charValue >= 23 && charValue <= 35){
				fromNtoZ[i] = a[i];
			}
		}
		
		return fromNtoZ;
	} //end of method to return N to Z

	public static void main(String[] args) { //Main method
		int randomNumber = (int) (Math.random() * 50 + 1); //Random array will be between length of 1 and 50
		int randomDigit = 0;
		String letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"; //Selection of letters to choose from
		
		char[] myArray = new char[randomNumber]; //Declares and initiates random array of length between 1 and 50
		
	
		System.out.print("Random character array: ");
		for(int i = 0; i < myArray.length; i++){ //Fills in slots of array and prints out their values
			randomDigit = (int)(Math.random() * 52);
			myArray[i] = letters.charAt(randomDigit);
			System.out.print(myArray[i]);
		}
		
		
		
		char[] AtoM = getAtoM(myArray);
		char[] NtoZ = getNtoZ(myArray);
		
		System.out.println();
		System.out.print("A to M characters: ");
		for (int i = 0; i < AtoM.length; i ++){ //Prints out all characters including and between A and M
			System.out.print(AtoM[i]);
		}
		
		System.out.println();
		System.out.print("N to Z characters: ");
		for (int i = 0; i < NtoZ.length; i ++){ //Prints out all characters including and between N and Z
			System.out.print(NtoZ[i]);
		}

		
		
	} //end of main method

} //end of class
