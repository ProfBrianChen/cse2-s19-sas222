////////
//// HW08, PlayLottery.java
//// Divides an array of letters into 2 separate arrays (A-M and N-Z)
//@author ssaini
//@version April 8, 2019
///

public class PlayLottery {

	public static boolean userWins(int[] user, int[] winning){ //Checks if users numbers and drawing numbers are same
		boolean decision = true;
		for (int i = 0; i < winning.length; i ++){
			if (user[i] != winning[i]){
				decision = false;
			}
		}
		return decision;
	}

	public static int[] numbersPicked(){ //Generates random numbers for lottery (w/out duplication)
		int[] drawing = new int[5];
		int randomNumber = 0;

		for(int i = 0; i < drawing.length; i ++){
			randomNumber = (int)(Math.random() * 60);
			drawing[i] = randomNumber;
			for(int j = 0; j < i; j ++){
				if (drawing[j] == drawing[i]){	//Prevents duplicates/replacement
					randomNumber = (int)(Math.random() * 60);
					drawing[i] = randomNumber;
				}
			}
		}

		return drawing;

	} //End of number-picking method

	public static void main(String[] args) {
		int [] myNumbers = {5, 19, 20, 21, 33};
		int [] drawing = numbersPicked();

		boolean weWon = userWins(myNumbers, drawing);

		//Print out both sets of numbers
		System.out.print("Your numbers: ");
		for(int i =0; i < myNumbers.length; i++){
			System.out.print(myNumbers[i] +" ");
		}

		System.out.println();
		System.out.print("Drawing's numbers: ");
		for(int i =0; i < drawing.length; i++){
			System.out.print(drawing[i] +" ");
		}

		//Print out decision
		System.out.println();
		if(weWon){
			System.out.println("HOLY MOLY! YOU WON!");
		}
		else{
			System.out.println("Sorry, you didn't win.");
		}


	}

}
