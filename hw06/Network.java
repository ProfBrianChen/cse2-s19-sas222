////////
//// hw06: Network.java
//// Makes a network of boxes
//@author ssaini
//@version March 19, 2019
///
import java.util.Scanner;

public class Network {
	public static void main(String[] args){
		Scanner	myScanner;
		boolean proceed = true;
		int height = -1;
		int width = -1;
		int squareSize = -1;
		int edgeLength = -1;

////Receive values for height, width, squareSize, and edgeLength
		do{	//For height
			myScanner = new Scanner (System.in);
			System.out.print("Please enter desired height: ");
			if (myScanner.hasNextInt()){
				height = myScanner.nextInt();
				if (height > 0){
					proceed = false;
				}
				else{
					System.out.println("Error. Try again.");
				}
			}
			else{
				System.out.println("Error. Try again.");
			}
		} while(proceed);
		proceed = true;

		do{	//For width
			myScanner = new Scanner (System.in);
			System.out.print("Please enter desired width: ");
			if (myScanner.hasNextInt()){
				width = myScanner.nextInt();
				if (width > 0){
					proceed = false;
				}
				else{
					System.out.println("Error. Try again.");
				}
			}
			else{
				System.out.println("Error. Try again.");
			}
		} while(proceed);
		proceed = true;

		do{	//For squareSize
			myScanner = new Scanner (System.in);
			System.out.print("Please enter square size: ");
			if (myScanner.hasNextInt()){
				squareSize = myScanner.nextInt();
				if (squareSize > 0){
					proceed = false;
				}
				else{
					System.out.println("Error. Try again.");
				}
			}
			else{
				System.out.println("Error. Try again.");
			}
		} while(proceed);
		proceed = true;

		do{	//For edgeLength
			myScanner = new Scanner (System.in);
			System.out.print("Please enter edge length: ");
			if (myScanner.hasNextInt()){
				edgeLength = myScanner.nextInt();
				if (edgeLength > 0){
					proceed = false;
				}
				else{
					System.out.println("Error. Try again.");		 
				}
			}
			else{
				System.out.println("Error. Try again.");	 
			}
		} while(proceed);
		proceed = true;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Printing time


		int y = 1;
		while(y <= height){ //Controls the column
			int x = 1;
			while (x <= width){ //controls the row
				if (y % squareSize == 1 || y % squareSize == 0){		
					if(x % squareSize == 1 || x % squareSize == 0){
						System.out.print("#");
					}
					else{
						System.out.print("-");
					}
					if (squareSize == 1){
						for (int i = 1; i<= edgeLength; i ++){System.out.print("-");}
					}

					if(x % squareSize == 0){
						for(int i = 1; i <= edgeLength; i ++){
							System.out.print(" ");
						}
					}


				} //end of if statement


				else{				
					if(x % squareSize == 1 || x % squareSize == 0){
						System.out.print("|");
					}
					else{
						System.out.print(" ");
					}

					if(x % squareSize == 0 && squareSize % 2 == 1){
						if (y % squareSize == squareSize / 2 + 1){
							for(int i = 1; i <= edgeLength; i ++){
								System.out.print("-");
							}
						}
						
						else{
							for(int i = 1; i <= edgeLength; i ++){
								System.out.print(" ");
							}
						}
					}

					else if (x % squareSize == 0 && squareSize % 2 == 0){
						if((y % squareSize == squareSize / 2 || y % squareSize == squareSize /2 + 1) || squareSize == 1){
							for (int i = 1; i <= edgeLength; i ++){
								System.out.print("-");
							}
						}
						else{
							for (int i = 1; i <= edgeLength; i ++){
								System.out.print(" ");
							}
						}
					}

				}
				x ++;

			}//end of inside for loop (row)
			System.out.println();

			//Vertical bars
			if (y % squareSize == 0 || squareSize == 1){
				for(int z = 1; z <= edgeLength; z++){
					if(squareSize % 2 == 1){
						for (int i = 1; i <= width; i ++){
							if ((i % squareSize == (squareSize / 2) + 1) || squareSize == 1){
								System.out.print("|");
							}
							else{
								System.out.print(" ");
							}
							
							if (i % squareSize == 0){
								for(int b = 1; b <= edgeLength; b ++){
									System.out.print(" ");
								}
							}
							
						}		
					}
					else {
						for (int i = 1; i <= width; i ++){
							if (i % squareSize == (squareSize / 2) ){
								System.out.print("||");
							}
							else{
								System.out.print(" ");
							}
							if (i % squareSize == 0){
								for(int b = 1; b <= edgeLength; b ++){
									System.out.print(" ");
								}
							}
							
						}

					}

					System.out.println();
				}

			}
			y ++;
		} //end of outside for loop (column)

	} //end of method
} //end of class