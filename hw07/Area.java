////////
//// hw07, Area
//// Takes input for the type of shape, takes input for the values of the sides of the shape, and prints out the area of the shape
//@author ssaini
//@version March 26, 2019
///
import java.util.Scanner;

public class Area {
	public static Scanner myScanner;
	public static String rect = "rectangle"; //String contains shortcut for rectangle
	public static String tri = "triangle"; //String contains shortcut for triangle
	public static String circle = "circle"; //String contains shortcut for circle

	public static double length = -1, width = -1, base = -1, height = -1, radius = -1; //Declares and initializes all dimensions 
  
	public static void Shape(String shape){ //Takes input for the values of dimensions
		boolean ready = true;

		//for a rectangle
		if(shape.equals(rect)){ 
			do{ //for length of rectangle
				myScanner = new Scanner(System.in);
				System.out.print("Please enter the length: ");
				if(myScanner.hasNextDouble()){
					length = myScanner.nextDouble();
					if (length > 0){ready = false;} 
					else{System.out.println("Error! Your value must be a number greater than 0. Please try again.");}
				}
				else{System.out.println("Error! Your value must be a number greater than 0. Please try again.");}
			}while(ready);

			ready = true;

			do{ //for width of rectangle
				myScanner = new Scanner(System.in);
				System.out.print("Please enter the width: ");
				if(myScanner.hasNextDouble()){
					width = myScanner.nextDouble();
					if (width > 0){ready = false;} 
					else{System.out.println("Error! Your value must be a number greater than 0. Please try again.");}
				}
				else{System.out.println("Error! Your value must be a number greater than 0. Please try again.");}
			}while(ready);

			System.out.println("The area of your triangle is: " + rectArea(length, width));
		}


		//For a triangle
		else if(shape.equals(tri)){
			do{ //for base of triangle
				myScanner = new Scanner(System.in);
				System.out.print("Please enter the base: ");
				if(myScanner.hasNextDouble()){
					base = myScanner.nextDouble();
					if (base > 0){ready = false;} 
					else{System.out.println("Error! Your value must be a number greater than 0. Please try again.");}
				}
				else{System.out.println("Error! Your value must be a number greater than 0. Please try again.");}
			}while(ready);

			ready = true;

			do{ //for height of triangle
				myScanner = new Scanner(System.in);
				System.out.print("Please enter the height: ");
				if(myScanner.hasNextDouble()){
					height = myScanner.nextDouble();
					if (height > 0){ready = false;} 
					else{System.out.println("Error! Your value must be a number greater than 0. Please try again.");}
				}
				else{System.out.println("Error! Your value must be a number greater than 0. Please try again.");}
			}while(ready);

			System.out.println("The area of your triangle is: " + triArea(base, height));
		}

		//For a circle
		else{
			do{ //for radius of circle
				myScanner = new Scanner(System.in);
				System.out.print("Please enter the radius: ");
				if(myScanner.hasNextDouble()){
					radius = myScanner.nextDouble();
					if (radius > 0){ready = false;} 
					else{System.out.println("Error! Your value must be a number greater than 0. Please try again.");}
				}
				else{System.out.println("Error! Your value must be a number greater than 0. Please try again.");}
			}while(ready);
			System.out.println("The area of your circle is: " + circleArea(radius));
		}
	} //end of Shape method

	public static double rectArea(double l, double w){
		double area = l * w;
		return area;
	}

	public static double triArea(double b, double h){
		double area = 0.5 * b * h;
		return area;
	}

	public static double circleArea(double r){
		double area = Math.PI * Math.pow(r, 2);
		return area;
	}

	public static void main(String[] args) {
		String myShape = "";
		String junkWord = "";
		do{
			myScanner = new Scanner (System.in);
			System.out.print("Your shape (rectangle, triangle, or circle): ");
			junkWord = myScanner.next().toLowerCase();

			if(junkWord.equals(rect) || junkWord.equals(tri) || junkWord.equals(circle)){
				myShape = junkWord;
			}
			else{
				System.out.println();
				System.out.println("Error! Your shape must be a rectangle, triangle, or circle. Please try again." );
				junkWord = "";
			}
		}while(!(junkWord.equals(rect) || junkWord.equals(tri) || junkWord.equals(circle)));

		Shape(myShape);

	}

}
