////////
//// hw07, StringAnalysis
//// Takes input from user and determines if all (or part) of the input are letters
//@author ssaini
//@version March 26, 2019
///

import java.util.Scanner;

public class StringAnalysis {
	public static boolean AnalyzeString(String userInput){ //If user chooses to scan the entire String
		boolean allLetters = true; //Boolean value that will be returned
		int lengthOfString = userInput.length();
		for(int i = 0; i < lengthOfString; i ++){
			if (!Character.isLetter(userInput.charAt(i))){
				allLetters = false;	
			}
		}
		return allLetters;
	} //end of AnalyzeString method
	
	public static boolean AnalyzeString(String userInput, int specifiedNumber){ //If user chooses to scan part of the String
		boolean allLetters = true; //Boolean value that will be returned
		if (specifiedNumber >= userInput.length()){
			specifiedNumber = userInput.length();
		}
		for(int i = 0; i < specifiedNumber; i ++){
			if (!Character.isLetter(userInput.charAt(i))){
				allLetters = false;
			}
		}
		return allLetters;
	} //end of AnalyzeString method
	
	public static void main(String[] args) { //Main method
		Scanner myScanner = new Scanner(System.in);
		System.out.print("Please enter a phrase: ");
		String userInput = myScanner.next(); //Contains user's phrase/String that will be tested
		System.out.println();
		System.out.println("Do you want to determine if your entire phrase consists of letters, or only a certain part?");
		String determine = "";
		String junkWord = ""; //Placeholder
		int n = -1; //
		boolean ready = true;
		do{
			myScanner = new Scanner(System.in);
			System.out.print("Enter 'All' for the former or 'Part' for the latter: ");
			junkWord = myScanner.next().toLowerCase();
			if(junkWord.equals("all") || junkWord.equals("part")){
				determine = junkWord;
				
				if (determine.equals("part")){
					do{
						myScanner = new Scanner(System.in);
						System.out.print("Please enter a positive integer: ");
						if (myScanner.hasNextInt()){
							n = myScanner.nextInt(); //Contains how many characters the user wants to check
							if(n > 0){ //Kept as greater than 0 to avoid run-time error
								ready = false;
							}
						}
					} while(ready);			//Continues to ask for input until the user gives correct input
				}
				
			}
		}while(!(junkWord.equals("all") || junkWord.equals("part")));
		
		//System.out.println(determine);
		//System.out.println(n);
		
		if(determine.equals("part")){
			if(AnalyzeString(userInput, n)){
				System.out.println("Part of your phrase is all letters,");
			}
			else{
				System.out.println("Your phrase is not entirely full of letters.");
			}
		}
		
		else{
			if(AnalyzeString(userInput)){
				System.out.println("Your entire phrase is full of letters." );
			}
			else{
				System.out.println("Your phrase is not entirely full of letters.");
			}	
		}
		

	} //end of main method

} //end of class