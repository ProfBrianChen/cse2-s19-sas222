////////
//// CSE 02 Hello World
//@author ssaini
//@version January 28, 2019
///
public class HelloWorld {
  
  public static void main(String[] args){
    //Prints Hello, World to terminal window
    System.out.println("Hello, World");
  
  }
  
}