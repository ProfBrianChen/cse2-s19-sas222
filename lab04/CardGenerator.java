////////
//// lab04: cardValueGenerator
////
/* Randomly selects a card from a standard 52-card deck
//@author ssaini
//@version February 15, 2019
*/
public class CardGenerator{
  public static void main(String[] args){
    String suit;    //name of suit
    String cardValue; //card/symbol of card
   
    int cardNumber = (int)((Math.random() * 52 + 1));    //Finds out where the card is in an ordered standard-52 deck 
    int suitNumber = ((cardNumber - 1) / 13);  //Finds what suit the card belongs in 
          //(cardNumber is subtracted by 1 because every 13th card in the deck is a King of the previous suit)
                                                   
    //Determines what suit the card belongs to
    switch(suitNumber){
      case 0: suit = "Diamonds"; break;
      case 1: suit = "Clubs";    break;  
      case 2: suit = "Hearts";   break;
      default: suit = "Spades";   break; 
    } //end of switch statement
      
    int cardSymbol = cardNumber % 13;     //Determines the card's face/number.
    switch(cardSymbol){
      case 1: cardValue = "Ace"; break;
      case 2: cardValue = "2"; break;
      case 3: cardValue = "3"; break;  
      case 4: cardValue = "4"; break;
      case 5: cardValue = "5"; break;
      case 6: cardValue = "6"; break;
      case 7: cardValue = "7"; break;
      case 8: cardValue = "8"; break;
      case 9: cardValue = "9"; break;
      case 10: cardValue = "10"; break;
      case 11: cardValue = "Jack"; break;
      case 12: cardValue = "Queen"; break;
      default: cardValue = "King";  break;
    } //end of switch method
    
    //Outputs what card has been selected
    System.out.println("You picked the " + cardValue + " of " + suit); 
    
  } //end of method
} //end of class