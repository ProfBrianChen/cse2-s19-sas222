import java.util.Scanner; //imports Scanner Class
////////
//// hw03: BoxVolume.java
//// Asks user for measurements of box dimensions, and calculates volume of box
//@author ssaini
//@version February 9, 2019
///

public class BoxVolume{
  public static void main(String[]args ){
    
    Scanner myScanner = new Scanner(System.in); //Initiates Scanner object
    
    //Gets user input (width)
    System.out.print("The width side of the box is: ");
    double width = myScanner.nextDouble();
    
    //Gets user input (length)
    System.out.print("The length of the box is: ");
    double length = myScanner.nextDouble();
    
    
    //Gets user input (heigh)
    System.out.print("The height of the box is: ");
    double height = myScanner.nextDouble();
    
    //Calculates the volume
    double volume = width * length * height;
   
    //An extra line-break for readability
    System.out.println();
    
    //Outputs the volume
    System.out.println("The volume inside the box is: " + (int)volume);
   
  } //end of main method
} //end of class