import java.util.Scanner; //imports Scanner Class
////////
//// hw03: Convert.java
//// Converts input number from meters to inches
//@author ssaini
//@version February 9, 2019
///

public class Convert{
  public static void main(String[]args ){
    
    Scanner myScanner = new Scanner(System.in); //Initiates Scanner object
    
    //Gets user input
    System.out.print("Enter the distance in meters: ");
    double meters = myScanner.nextDouble(); //User input is stored as a distance in meters
    
    //Conversions and calculations
    double centimeters = meters * 100;  //Conversion from meters to centimeters
    double inches = centimeters / 2.54; //Conversion from centimeters to inches
    
    //Outputs with 4 decimal places 
    System.out.print(meters + " meters is " );
    System.out.printf("%5.4f", inches); 
    System.out.println(" inches.");
    
   
  } //end of main method
} //end of class