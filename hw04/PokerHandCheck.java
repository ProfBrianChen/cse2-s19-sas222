////////
//// hw04: PokerHandCheck.java
//// Randomly selects 5 cards and declares whether the hand is a pair, 2 pair, 3-of-a-kind, 3-of-a-kind and 1 pair, or high card hand
//@author ssaini
//@version February 19, 2019
///

public class PokerHandCheck{
  public static void main(String[] args){
	System.out.println("The cards in your hand are: ");
	String suit;    //name of suit
    String cardValue; //card/symbol of card
    int cardNumber;   //the location of a random card in a standard-52 deck
    int suitNumber;  //Determines what suit a randomly selected card belongs in 
    
    //Randomly selecting card 1
    cardNumber = (int)((Math.random() * 52 + 1));   
    suitNumber = ((cardNumber - 1) / 13); 
  
    //Determines what suit the card belongs to
    switch(suitNumber){
      case 0: suit = "Diamonds"; break;
      case 1: suit = "Clubs";    break;  
      case 2: suit = "Hearts";   break;
      default: suit = "Spades";   break; 
    } //end of switch statement
      
    int cardSymbol1 = cardNumber % 13;     //Determines the card's face/number.
    switch(cardSymbol1){
      case 1: cardValue = "Ace"; break;
      case 2: cardValue = "2"; break;
      case 3: cardValue = "3"; break;  
      case 4: cardValue = "4"; break;
      case 5: cardValue = "5"; break;
      case 6: cardValue = "6"; break;
      case 7: cardValue = "7"; break;
      case 8: cardValue = "8"; break;
      case 9: cardValue = "9"; break;
      case 10: cardValue = "10"; break;
      case 11: cardValue = "Jack"; break;
      case 12: cardValue = "Queen"; break;
      default: cardValue = "King";  break;
    } //end of switch method
    
    //Outputs what card has been selected
    System.out.println("\t the " + cardValue + " of " + suit); 
    
    
    //Randomly selecting card 2
    cardNumber = (int)((Math.random() * 52 + 1));   
    suitNumber = ((cardNumber - 1) / 13); 
  
    //Determines what suit the card belongs to
    switch(suitNumber){
      case 0: suit = "Diamonds"; break;
      case 1: suit = "Clubs";    break;  
      case 2: suit = "Hearts";   break;
      default: suit = "Spades";   break; 
    } //end of switch statement
      
    int cardSymbol2 = cardNumber % 13;     //Determines the card's face/number.
    switch(cardSymbol2){
      case 1: cardValue = "Ace"; break;
      case 2: cardValue = "2"; break;
      case 3: cardValue = "3"; break;  
      case 4: cardValue = "4"; break;
      case 5: cardValue = "5"; break;
      case 6: cardValue = "6"; break;
      case 7: cardValue = "7"; break;
      case 8: cardValue = "8"; break;
      case 9: cardValue = "9"; break;
      case 10: cardValue = "10"; break;
      case 11: cardValue = "Jack"; break;
      case 12: cardValue = "Queen"; break;
      default: cardValue = "King";  break;
    } //end of switch method
    
    //Outputs what card has been selected
    System.out.println("\t the " + cardValue + " of " + suit); 
    
    
    //Randomly selecting card 3
    cardNumber = (int)((Math.random() * 52 + 1));   
    suitNumber = ((cardNumber - 1) / 13); 
  
    //Determines what suit the card belongs to
    switch(suitNumber){
      case 0: suit = "Diamonds"; break;
      case 1: suit = "Clubs";    break;  
      case 2: suit = "Hearts";   break;
      default: suit = "Spades";   break; 
    } //end of switch statement
      
    int cardSymbol3 = cardNumber % 13;     //Determines the card's face/number.
    switch(cardSymbol3){
      case 1: cardValue = "Ace"; break;
      case 2: cardValue = "2"; break;
      case 3: cardValue = "3"; break;  
      case 4: cardValue = "4"; break;
      case 5: cardValue = "5"; break;
      case 6: cardValue = "6"; break;
      case 7: cardValue = "7"; break;
      case 8: cardValue = "8"; break;
      case 9: cardValue = "9"; break;
      case 10: cardValue = "10"; break;
      case 11: cardValue = "Jack"; break;
      case 12: cardValue = "Queen"; break;
      default: cardValue = "King";  break;
    } //end of switch method
    
    //Outputs what card has been selected
    System.out.println("\t the " + cardValue + " of " + suit); 
    
    
    //Randomly selecting card 4
    cardNumber = (int)((Math.random() * 52 + 1));   
    suitNumber = ((cardNumber - 1) / 13); 
  
    //Determines what suit the card belongs to
    switch(suitNumber){
      case 0: suit = "Diamonds"; break;
      case 1: suit = "Clubs";    break;  
      case 2: suit = "Hearts";   break;
      default: suit = "Spades";   break; 
    } //end of switch statement
      
    int cardSymbol4 = cardNumber % 13;     //Determines the card's face/number.
    switch(cardSymbol4){
      case 1: cardValue = "Ace"; break;
      case 2: cardValue = "2"; break;
      case 3: cardValue = "3"; break;  
      case 4: cardValue = "4"; break;
      case 5: cardValue = "5"; break;
      case 6: cardValue = "6"; break;
      case 7: cardValue = "7"; break;
      case 8: cardValue = "8"; break;
      case 9: cardValue = "9"; break;
      case 10: cardValue = "10"; break;
      case 11: cardValue = "Jack"; break;
      case 12: cardValue = "Queen"; break;
      default: cardValue = "King";  break;
    } //end of switch method
    
    //Outputs what card has been selected
    System.out.println("\t the " + cardValue + " of " + suit); 
    
    
    //Randomly selecting card 5
    cardNumber = (int)((Math.random() * 52 + 1));   
    suitNumber = ((cardNumber - 1) / 13); 
  
    //Determines what suit the card belongs to
    switch(suitNumber){
      case 0: suit = "Diamonds"; break;
      case 1: suit = "Clubs";    break;  
      case 2: suit = "Hearts";   break;
      default: suit = "Spades";   break; 
    } //end of switch statement
      
    int cardSymbol5 = cardNumber % 13;     //Determines the card's face/number.
    switch(cardSymbol5){
      case 1: cardValue = "Ace"; break;
      case 2: cardValue = "2"; break;
      case 3: cardValue = "3"; break;  
      case 4: cardValue = "4"; break;
      case 5: cardValue = "5"; break;
      case 6: cardValue = "6"; break;
      case 7: cardValue = "7"; break;
      case 8: cardValue = "8"; break;
      case 9: cardValue = "9"; break;
      case 10: cardValue = "10"; break;
      case 11: cardValue = "Jack"; break;
      case 12: cardValue = "Queen"; break;
      default: cardValue = "King";  break;
    } //end of switch method
    
    //Outputs what card has been selected
    System.out.println("\t the " + cardValue + " of " + suit); 
    
    
////////////////////////////////////////////////////////////////////////////////////////////////////
//Matching a determining whether you have a pair, two pair, three of a kind, or a high end card  
    int pairCount = 0;
    
    //Series of if statements that compares value of Card 1 with the others
    if(cardSymbol1 == cardSymbol2){pairCount ++;}
    if(cardSymbol1 == cardSymbol3){pairCount ++;}
    if(cardSymbol1 == cardSymbol4){pairCount ++;}
    if(cardSymbol1 == cardSymbol5){pairCount ++;}
    
    //Series of if statements that compares value of Card 2 with the others (except Card 1)
    if(cardSymbol2 == cardSymbol3){pairCount ++;}
    if(cardSymbol2 == cardSymbol4){pairCount ++;}
    if(cardSymbol2 == cardSymbol5){pairCount ++;}
    
    //Series of if statements that compares value of Card 3 with the others (except Cards 1 and 2)
    if(cardSymbol3 == cardSymbol4){pairCount ++; }
    if(cardSymbol3 == cardSymbol5){pairCount ++;}

    //if statement that compares value of Card 4 with Card 5
    if(cardSymbol4 == cardSymbol5){pairCount ++;}
 
    System.out.println(); //line break for readability
    
    switch(pairCount){
	    case 1: System.out.println("You have a pair!"); break;
	    case 2: System.out.println("You have 2 pairs!"); break;
	    case 3: System.out.println("You have three-of-a-kind!"); break;
	    case 4: System.out.println("What luck! You have 1 pair and three-of-a-kind!"); break;
      case 6: System.out.println("Nice! You have four-of-a-kind!"); break;
	    default: System.out.println("You have a high card hand!"); break;    
    } //End of switch statement   
 
  } //end of main method
} //end of class