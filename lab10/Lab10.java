import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Scanner;
////////
//// Lab10
//// Takes input for the number of rows and columns in a matrix, and fills them accordingly
///Searches for specific value in array via binary search or linear search
//@author ssaini
//@version April 19, 2019
///
public class Lab10 {
	public static int[][] increasingMatrix(int width, int height, boolean format){
		int[][] ourMatrix = new int[width][height];
		int i = 1;
		if (format == true){ //row major representation
			for (int x = 0; x < width; x ++){
				for(int y = 0; y < height; y ++){
					ourMatrix[x][y] = i;
					i++;
				}
			}
		}
		else{
			for (int y = 0; y < height; y ++){
				for(int x = 0; x < width; x ++){
					ourMatrix[x][y] = i;
					i++;
				}
			}
		}
		return ourMatrix;
	} //end of increasingMatrix method

	public static void printMatrix(int[][] array, boolean format) {
		if(array == null){
			System.out.println("The array was empty!");
		}
		else{
			if (format == true){
				for (int i = 0; i < array.length; i ++){
					for (int j = 0; j < array[i].length; j ++){
						System.out.print(array[i][j] + " ");
					}
					System.out.println();
				}
			}
			else{
				array = translate(array);
				printMatrix(array, true);
			}
		}
	} //end of printMatrix method

	public static int[][] translate(int[][] theArray){
		int[] arrayValues = new int[theArray.length * theArray[0].length];
		int[] [] theNewArray = new int[theArray.length][theArray[0].length];
		int n = 0;
		for(int i = 0; i < theArray[0].length; i ++){
			for (int j = 0; j < theArray.length; j ++){
				arrayValues[n] = theArray[j][i];
				n++;
			}
		}

		n = 0;
		for (int i = 0; i < theNewArray.length; i ++){
			for (int j = 0; j < theNewArray[0].length; j++){
				theNewArray[i][j] = arrayValues[n];
				n ++;
			}
		}

		return theNewArray;
	} //end of translate method


	public static int[][] addMatrix(int[][] matrixA, boolean formatA, int[][] matrixB, boolean formatB) {
		int[][] newMatrix;
		if (matrixA.length == matrixB.length && matrixA[0].length == matrixB[0].length){
			if (formatA == false){
				matrixA = translate(matrixA);
			}
			if (formatB == false){
				matrixB = translate(matrixB);
			}


			newMatrix = new int[matrixA.length][matrixA[0].length];
			for (int i = 0; i < matrixA.length; i++){
				for (int j = 0; j < matrixA[0].length; j++){
					newMatrix[i][j] = matrixA[i][j] + matrixB[i][j];
				}
			}
		}
		else{
			newMatrix = null;
			System.out.println("The arrays could not be added!");
		}
		return newMatrix;
	} //end of add Matrices method

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int width1 = (int)(Math.random() * 10) + 1;
		int width2 = (int)(Math.random() * 10) + 1;
		int height1 = (int)(Math.random() * 10) + 1;
		int height2 = (int)(Math.random() * 10) + 1;
		
		int[][] matrixA = increasingMatrix(width1, height1, true);
		int[][] matrixB = increasingMatrix(width1, height1, false);
		int[][] matrixC = increasingMatrix(width2, height2, true);
		System.out.println("Matrix A: ");
		printMatrix(matrixA, true);
		System.out.println();
		System.out.println("Matrix B (translated): ");
		printMatrix(matrixB, false);
		System.out.println();
		
		System.out.println("Sum of Matrix A and B: ");
		int[][] sumOfAB = addMatrix(matrixA, true, matrixB, false);
		printMatrix(sumOfAB, true);
		
		System.out.println();
		System.out.println("Matrix C: ");
		printMatrix(matrixC, true);
		
		System.out.println();
		System.out.println("Sum of Matrix A and C: ");
		int[][] sumOfAC = addMatrix(matrixA, true, matrixC, true);
		printMatrix(sumOfAC, true);
		
		
		/* My test
		int[][] myMatrix = increasingMatrix(9, 7, true);
		printMatrix(myMatrix, true);
		System.out.println();
		int[][] otherMatrix = increasingMatrix(9, 7, false);	//format is always false from here on for this matrix
		printMatrix(otherMatrix, false);
		System.out.println();

		//things get complicated
		int[][] ourMatrix = addMatrix(myMatrix, true, otherMatrix, false);
		printMatrix(ourMatrix, true);

		 */
	} //end of main method

} //end of class
