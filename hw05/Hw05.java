////////
//// HW05: Asks for user data
/*		   Determines if inputed data is of the right type
 * 		   Continues to ask for input until right type of data is inputed
 * 		   Outputs data in a table-like format
 */
//@author ssaini
//@version March 3, 2019

import java.util.Scanner;

public class Hw05{
	public static void main(String[] args){
		Scanner myScanner;

		int courseNumber = -1;
		String departmentName = "";
		int meetingFrequency = -1;
		String startTime = "";
		String instructorName = "";
		int numberOfStudents = -1;

		boolean ready;

		///////////////////////////////////////////////////////
		//Loop for courseNumber
		ready = true;
		do{
			myScanner = new Scanner(System.in);
			System.out.print("Please enter the course-number (as an integer): ");
			if (myScanner.hasNextInt()){
				courseNumber = myScanner.nextInt();
				if(courseNumber >= 0){
					ready = false;
				}
			}
		} while(ready);			//Continues to ask for input until the user gives correct input 


		///////////////////////////////////////////////////////
		//Loop for departmentName
		ready = true;
		do{
			myScanner = new Scanner(System.in);
			System.out.print("Please enter the department name (as a String): ");
			if (myScanner.hasNext()){
				departmentName = myScanner.nextLine();				
				ready = false;
			}
		} while(ready);			//Continues to ask for input until the user gives correct input


		///////////////////////////////////////////////////////
		//Loop for meetingFrequency
		ready = true;
		do{
			myScanner = new Scanner(System.in);
			System.out.print("Please enter the number of times you meet in this class (as an integer): ");
			if (myScanner.hasNextInt()){
				meetingFrequency = myScanner.nextInt();
				if(meetingFrequency >= 1){
					ready = false;
				}
			}
		} while(ready);			//Continues to ask for input until the user gives correct input 


		///////////////////////////////////////////////////////
		//Loop for startTime
		ready = true;
		do{
			myScanner = new Scanner(System.in);
			System.out.print("Please enter the starting time for this class (as a String): ");
			if (myScanner.hasNext()){
				startTime = myScanner.nextLine();				
				ready = false;
			}
		} while(ready);			//Continues to ask for input until the user gives correct input


		///////////////////////////////////////////////////////
		//Loop for instructorName
		ready = true;
		do{
			myScanner = new Scanner(System.in);
			System.out.print("Please enter the name of your instructor (as a String): ");
			if (myScanner.hasNext()){
				instructorName = myScanner.nextLine();				
				ready = false;
			}
		} while(ready);			//Continues to ask for input until the user gives correct input


		///////////////////////////////////////////////////////
		//Loop for numberOfStudents
		ready = true;
		do{
			myScanner = new Scanner(System.in);
			System.out.print("Please enter the total number of students in this class (as an integer): ");
			if (myScanner.hasNextInt()){
				numberOfStudents = myScanner.nextInt();
				if(numberOfStudents >= 1){
					ready = false;
				}
			}
		} while(ready);			//Continues to ask for input until the user gives correct input 
		System.out.println();
		
		System.out.println("Course Number \t   Department   \t   Number of Meetings  \t \t Starting Time  \t  Instructor  \t \t  Number of Students  \t");
		System.out.println("--------------------------------------------------------------------------------------------------------------------------------------------");
		System.out.println(courseNumber + "    \t \t     \t" + departmentName + "\t    \t \t  \t" + meetingFrequency + " \t \t  \t " + startTime + "\t     \t " + instructorName + "  \t  \t \t" + numberOfStudents);
		
		System.out.println();

	}//end of main method 

}//end of class