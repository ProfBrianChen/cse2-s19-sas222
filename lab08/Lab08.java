////////
//// Lab08, Arrays
//// Takes input for the number of slots in an array and inserts random integers in slots
	///Prints out all values of array
	//Prints and calculates the range, mean, and standard deviation of values in array
	//Sorts array and then shuffles array
//@author ssaini
//@version March 26, 2019
///

import java.util.Arrays;


public class Lab08 {
	
	public static int getRange(int[] a){ //Finds the difference between max and min values of the array/data value
		Arrays.sort(a); //Sorts array into ascending order
		int lengthOfArray = a.length;
		int maxValue = a[lengthOfArray - 1];
		int minValue = a[0];
		
		int range = maxValue - minValue;
		return range;
	}//end of getRange method
	
	public static double getMean(int[] a){ //Calculates and returns the mean of the array/data value
		int lengthOfArray = a.length;
		double total = 0;
		for(int i = 0; i < lengthOfArray; i ++){
			total = total + a[i];
		}
		double mean = total / lengthOfArray;
		return mean;
	} //end of getMean method
	
	
	public static double getStdDev(int[] a){ //Calculates and returns the standard deviation of the array/data
		double mean = getMean(a);
		double stdDev = -1;
		int lengthOfArray = a.length;
		double toBeSquared = 0;
		for (int i = 0; i < lengthOfArray; i ++){
			toBeSquared = toBeSquared + Math.pow(a[i] - mean, 2);
		}
		stdDev = Math.sqrt(toBeSquared / (lengthOfArray - 1));
		return stdDev;
	} //end of getStdDev method
	
	
	public static void shuffle(int[] a){ //Shuffles the array
		int lengthOfArray = a.length;
		int[] b = new int[lengthOfArray];
		for(int i = 0; i < lengthOfArray; i ++){
			b[i] = i;
		}
		
		for(int i = 0; i < lengthOfArray; i ++){
			int chooser = (int)(Math.random() * lengthOfArray);
			int helper = a[i];
			a[i] = a[b[chooser]];
			a[b[chooser]] = helper;
			lengthOfArray --;
		}
	} //shuffles an array
	
	
	public static void main(String[] args) { //Main Method
		double randomDouble = Math.random();
		randomDouble = randomDouble * 50 + 51;	//The random size is within [50, 100]
		int randomInt = (int)randomDouble;
		
		int lengthOfArray = randomInt;
		int[] arraysOfInt = new int[lengthOfArray];
		System.out.println("Size of the array is: " + lengthOfArray);
		
		for(int i = 0; i < lengthOfArray; i++){ //Fills array with values within [0, 99]		
			randomInt = (int)(Math.random() * 100);
			arraysOfInt[i] = randomInt;
			System.out.print(arraysOfInt[i] + " ");	
		}
		
		System.out.println();		
		System.out.println();
		System.out.println("The range for this array is: " + getRange(arraysOfInt));
		System.out.println("The mean for this array is: " + getMean(arraysOfInt));
		System.out.println("The standard deviation for this array is: " + getStdDev(arraysOfInt));
		
		System.out.println();
		System.out.println("Here are the values after shuffling the array:");
		shuffle(arraysOfInt);
		for(int i = 0; i < lengthOfArray; i++){
			System.out.print(arraysOfInt[i] + " ");	
		}	
    System.out.println();
				
	} //end of main method

} //end of class
