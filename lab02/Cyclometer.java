////////
//// lab02: Cyclometer
//// Prints out the number of minutes, counts, and miles for each trip.
//// Prints out the distance for the two trips combined.
//@author ssaini
//@version February 2, 2019
///

public class Cyclometer {
	public static void main(String[] args){
		//Input data
		double secsTrip1 = 480; 	//The number of seconds of Trip 1
		double secsTrip2 = 3220; // The number of seconds of Trip 2

		int countsTrip1 = 1561; //Number of full rotations of the front wheel for Trip 1
		int countsTrip2 = 9037; //Number of full rotations of the front wheel for Trip 2


		//Variables for constants and conversion factors
		double wheelDiameter = 27.0;	 //Diameter of wheel (in inches)
		double PI = 3.14159;				 // Constant for pi
		int feetPerMile = 5280;	 	//Conversion factor (miles —> feet)
		int inchesPerFoot = 12;		//Conversion factor (feet —> inches)
		int secondsPerMinute = 60; 		// Conversion factor (minutes —> seconds)
		
		double distanceTrip1;			//Distance traveled throughout Trip 1
		double distanceTrip2;			//Distance traveled throughout Trip 2
		double totalDistance; 			//Total distance traveled


		//Output (minutes are converted)
		System.out.println("Trip 1 took " + (secsTrip1 / secondsPerMinute) + " minutes and had " + countsTrip1 + " counts.");
		System.out.println("Trip 2 took " + (secsTrip2 / secondsPerMinute) + " minutes and had " + countsTrip2 + " counts.");


		//Runs the calculations and stores the values. 
		// Calculates the distance of the trip in inches
		distanceTrip1 = countsTrip1 * wheelDiameter * PI;
		distanceTrip2 = countsTrip2 * wheelDiameter * PI;			
		
		//Converts the distance of the trip in miles
		distanceTrip1 = distanceTrip1 / (inchesPerFoot * feetPerMile) ;	
		distanceTrip2 = distanceTrip2 / (inchesPerFoot * feetPerMile) ;
		totalDistance = distanceTrip1 + distanceTrip2;

		//Output (distance of both trips in miles)
		System.out.println("Trip 1 was " + distanceTrip1 + " miles");
		System.out.println("Trip 2 was " + distanceTrip2 + " miles");
		System.out.println("The total distance was " + totalDistance + " miles");


	} //end of main method

} //end of class
