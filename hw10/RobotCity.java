import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Scanner;
////////
//// HW10: RobotCity.java
//// Builds a city of several blocks
	///@method invade(): Robots land in the city
	///@method update(): Robots are pushed eastwards
//@author ssaini
//@version April 30, 2019
///
public class RobotCity {
	public static int[][] buildCity(){
		int x = (int)(Math.random() * 6) + 10;
		int y = (int)(Math.random() * 6) + 10;
		int[][] ourCity =  new int[x][y];
		for (int i = 0; i < x; i ++){
			for (int j = 0; j < y; j ++){
				ourCity[i][j] = (int)(Math.random() * 900) + 100;
			}
		}
		return ourCity;
	}//end of buildCity method
	
	public static void display(int[][] theCity){
		int x = theCity.length;
		int y = theCity[0].length;
		for (int i = 0; i < x; i ++){
			for (int j = 0; j < y; j ++){
				System.out.printf("%4d ", theCity[i][j]);
			}
			System.out.println();
		}
	} //end of display method
	
	public static int[][] invade(int[][] theCity, int k){
		int x = 0;
		int y = 0;
		
		for(int i = 0; i < k; i++){
			boolean ready = false;
			do{
				x = (int)(Math.random() * (theCity.length));
				y = (int)(Math.random() * (theCity[0].length));
				if (theCity[x][y] > 0){
					theCity[x][y] = -1 * theCity[x][y];
					ready = true;
				}
			}while(!ready);
		}
		
		return theCity;
	} //end of invade method
	
	
	
	public static int[][] update(int[][] theCity){
		int x = theCity.length;
		int y = theCity[0].length;
		int[][] theNewCity = new int[x][y];
		
		for(int i = 0; i < x; i ++){
			for(int j = 0; j < y; j++){
				if (j != 0){
					if (theCity[i][j - 1] < 0){
						theNewCity[i][j] = -1 * Math.abs(theCity[i][j]);
					}
					else{
						theNewCity[i][j] = Math.abs(theCity[i][j]);
					}
				}
				else{
					theNewCity[i][j] = Math.abs(theCity[i][j]);
				}
				
			} //end of inner for
		} //end of outer for
		theCity = theNewCity;
		return theCity;
			
		
	} //end of update method
	
	
	
	public static void main(String[] args) {
		int[][] myCity = buildCity();
		int numberOfRobots = (int)(Math.random() * myCity.length * myCity[0].length);
		System.out.println("Here is your city: ");
		display(myCity);
		myCity = invade(myCity, numberOfRobots);
		System.out.println();		
		System.out.println(numberOfRobots + " robots invaded the city!");
		System.out.println();
		display(myCity);
		for (int i = 0; i < 5; i++){
			myCity = update(myCity);
			System.out.println();		
			System.out.println("Update: " + (i + 1));
			System.out.println();
			display(myCity);
		}
			
	} //end of main method

} //end of class
