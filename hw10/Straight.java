import java.lang.reflect.Array;
import java.util.Arrays;

////////
//// HW10: Straight.java
//// Checks if a selected hand of cards are a straight
//@author ssaini
//@version May 1, 2019
public class Straight {
	
	public static int[] shuffleDeck(){
		int[] myDeck = new int [52];
		for (int i = 0; i < 52; i++){
			myDeck[i] = i;
		}
		for (int i = 0; i < myDeck.length; i++) {
			//find a random member to swap with
			int target = (int) (myDeck.length * Math.random() );

			//swap the values
			int temp = myDeck[target];
			myDeck[target] = myDeck[i];
			myDeck[i] = temp;
		}		
		return myDeck;
	} //end of shuffleDeck method
	
	public static int[] myHand(int[] shuffledDeck){
		int[] myHand = new int[5];
		for (int i = 0; i < myHand.length; i ++){
			myHand[i] = shuffledDeck[i];
		}
		
		return myHand;
	} //end of myHand method
	
	public static int checkHand(int[] myHand, int k){
		int cardNumber = 0;
		int smallest = 0;
		int[] sortedArray = new int[myHand.length];
		for (int i = 0; i < myHand.length; i++){
			sortedArray[i] = myHand[i];
		}
		if (k > 5 || k <= 0){
			cardNumber = -1;
		}
		else{
			Arrays.sort(sortedArray);
			cardNumber = sortedArray[k - 1];
		}
		return cardNumber;
	}
	
	public static boolean straight(int[] myHand){
		boolean straight = true;
		int[] sortedArray = new int[myHand.length];
		for (int i = 0; i < myHand.length; i++){
			sortedArray[i] = myHand[i];
		}
		
		Arrays.sort(sortedArray);
		for (int i = 1; i < myHand.length; i ++){
			if(sortedArray[i -1] + 1 != sortedArray[i]){
				straight = false;
			}
		}
		return straight;
	}
	
	public static void main(String[] args) {
		int numberOfStraights = 0;
		int [] theDeck;
		int [] theHand;
		double million = 1000000.0;
		for (int i = 0; i < million; i ++){
			theDeck = shuffleDeck();
			theHand = myHand(theDeck);
			if (straight(theHand)){
				numberOfStraights ++;
			}
		}
		
		System.out.println("You had " + numberOfStraights + " number of straights.");
		System.out.println("Probability of having a straight is: " + (100 * numberOfStraights / million) + "%");
		
	} //end of main method

} //end of class
