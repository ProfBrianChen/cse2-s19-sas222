import java.util.Scanner;   //Imports Scanner class
////////
//// lab05: TwistGenerator.java
////
/* Prints out a series of slashes and X's of a user-specified length
//@author ssaini
//@version March 1, 2019
*/

public class TwistGenerator{
  public static void main(String[] args){
		Scanner myScanner;  //Declares and initializes Scanner object
		boolean ready = true; //Declares and initializes a boolean object
		int posInt = -1;    //Declares and temporarily initializes the length of the twist
		do{
			myScanner = new Scanner(System.in);
			System.out.print("Please enter a positive integer: ");
			if (myScanner.hasNextInt()){
				posInt = myScanner.nextInt();
				if(posInt >= 0){
					ready = false;
				}
			}
		} while(ready);			//Continues to ask for input until the user gives correct input
		System.out.println();
		
		
		///////////////////////// Prints out the table-like data

    for(int i = 1; i <= posInt; i ++){
					switch(i % 3){
						case 1: System.out.print("\\"); break;
						case 2: System.out.print(" "); break;
						default: System.out.print("/"); break;
					} //closes switch statement
				} //closes for loop
  
    System.out.println();
    
    for(int i = 1; i <= posInt; i ++){
      switch(i % 3){
        case 2: System.out.print("X"); break;
        default: System.out.print(" "); break;
      } //closes switch statement
    } //closes for loop	

    System.out.println(); //line-break

    for(int i = 1; i <= posInt; i ++){
      switch(i % 3){
        case 1: System.out.print("/"); break;
        case 2: System.out.print(" "); break;
        default: System.out.print("\\"); break;
      } //closes switch statement
    } //closes for loop
		
    System.out.println(); //line-break
			
	} // end of method
} //end of class