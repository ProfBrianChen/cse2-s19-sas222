////////
//// lab06, Display Triangles: Pattern A.java
//// Asks the user for input, then prints out a triangle of numbers
//@author ssaini
//@version March 18, 2019
///
import java.util.Scanner;


public class PatternA {
	public static void main(String[] args) {
		Scanner myScanner;
		int n = 0;
		boolean ready = true;
		do{
			System.out.print("Please enter a whole number greater than 0: ");
			myScanner = new Scanner (System.in);
			if(myScanner.hasNextInt()){
				n = myScanner.nextInt();
        if(n > 0){
         ready = false; 
        }
			}
		}while(ready); 
		
		System.out.println();
		
		for (int i = 1; i <= n; i ++){
			for (int z = 1; z <= i; z ++){
				System.out.print(z + " ");
			}
			System.out.println();
		}
	} //end of method
}//end of class
