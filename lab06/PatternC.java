////////
//// lab06, Display Triangles: Pattern C.java
//// Asks the user for input, then prints out a triangle of numbers
//@author ssaini
//@version March 18, 2019
///
import java.util.Scanner;


public class PatternC {
	public static void main(String[] args) {
		Scanner myScanner;
		int n = 0;
		boolean ready = true;
		do{
			System.out.print("Please enter a whole number greater than 0: ");
			myScanner = new Scanner (System.in);
			if(myScanner.hasNextInt()){
				n = myScanner.nextInt();
				if(n > 0){
         ready = false; 
        }
			}
		}while(ready); 
		System.out.println();
		
		String printThis = ""; //String for numbers
		String spaces = ""; //separate string for right justification
		
		for (int i = 1; i <= n; i ++){ //goes through each line
			
			for(int k = 1; k <= (n - i); k ++){
				spaces = spaces + " ";
			} //end of loop for spaces
			
			for (int z = 1; z <= i; z ++){
				printThis = z + printThis;
			} //end of string that puts numbers together
			
			printThis = spaces + printThis; //combines spaces and numbers
			
			System.out.print(printThis);
			
			printThis = ""; //resets number string
			spaces = "";  //resets spaces
			
			
			System.out.println();
		}
	} //end of method
}//end of class
