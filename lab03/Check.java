import java.util.Scanner;     //Imports Scanner method for user Input
////////
//// lab03: Check
////
/*  Takes input from user:
      The cost of their dinner
      The percentage of their tip
      The number of people the check will be split with
*/
//@author ssaini
//@version February 8, 2019
///

public class Check{
  public static void main(String[]args ){
    
    //Cost of the check
    Scanner myScanner = new Scanner(System.in);
    System.out.print("Enter the cost of the check in the following form xx.xx: ");
    double checkCost = myScanner.nextDouble();
    
    //Tip percentage
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the format xx): ");
    double tipPercent = myScanner.nextDouble();
    tipPercent = tipPercent / 100;  //Converts the tip percentage into a decimal value
    System.out.println();
    
    //People who pay the check
    System.out.println("Enter the number of people who will pay the check: ");
    int numPeople = myScanner.nextInt();
    
    double totalCost = checkCost + (checkCost * tipPercent);
    double costPerPerson = totalCost / numPeople;
    
    int dollars, //whole dollar amount
    dimes, //tens-place
    pennies; //ones-place
    
    dollars = (int)costPerPerson;
    dimes = (int)(costPerPerson * 10) % 10;
    pennies = (int)(costPerPerson * 100) % 10;
    
    System.out.println("Each person owes: " + dollars + "." + dimes + pennies);
    
    
  }   //end of main method
}   //end of class
