////////
//// hw02: Arithmetic
//// Computes the cost of to-be-purchased items (with 6% sales tax) in specific outputs:
/*		Total cost of each kind of item (i.e. total cost of pants, etc)
		Sales tax charged buying all of each kind of item (i.e. sales tax charged on belts)
		Total cost of purchases (before tax)
		Total sales tax
		Total paid for this transaction, including sales tax. */
//@author ssaini
//@version February 2, 2019
///

public class Arithmetic {
	public static void main(String[] args){
		int numPants = 3; 	//Number of pairs of pants
		double pantsPrice = 34.98;		// Cost per pair of pants
		
		int numShirts = 2; 			// Number of sweatshirts
		double shirtPrice = 24.99; 		// Cost per shirt

		int numBelts = 1; 			// Number of belts
		double beltPrice = 33.99;		// Cost per belt

		double paSalesTax = 0.06;		// Tax rate

		//Total cost for each group of items
		double totalCostOfPants = numPants * pantsPrice;	  //total cost of pants
		double totalCostOfShirts = numShirts * shirtPrice; //total cost of shirts
		double totalCostOfBelts = numBelts * beltPrice; //total cost of belts
		
		//Output: Summary of pants
		System.out.println("The subtotal for pants is: $" + totalCostOfPants);
		System.out.printf("The total sales tax for this item is: $" + "%-5.2f", (totalCostOfPants * paSalesTax));
		System.out.println();
		System.out.println();

		//Output: Summary of shirts
		System.out.println("The subtotal for shirts is: $" + totalCostOfShirts);
		System.out.printf("The total sales tax for this item is: $" + "%-5.2f", (totalCostOfShirts * paSalesTax));
		System.out.println();
		System.out.println();

		//Output: Summary of belts
		System.out.println("The subtotal for belts is: $" + totalCostOfBelts);
		System.out.printf("The total sales tax for this item is: $" + "%-5.2f", (totalCostOfBelts * paSalesTax));
		System.out.println();		
		System.out.println();
		
		//Output: Subtotal, total sales tax, total
		double totalCost = totalCostOfPants + totalCostOfShirts + totalCostOfBelts;
		System.out.println("Subtotal (before taxes): $" + totalCost);
		System.out.printf("Total sales tax: $" + "%-5.2f", (totalCost * paSalesTax));
		System.out.println();
		System.out.printf("Total (subtotal and sales tax): $" + "%-5.2f", (totalCost + (totalCost *paSalesTax))); 
		
	} //end of main method


} //end of class