//////////
//// CSE 002 WelcomeClass
//@author ssaini
//@version January 28, 2019
//
public class WelcomeClass{
  
  public static void main (String[] args){
    //Prints WelcomeClass with special characters and titles
    System.out.println("  -----------");
    System.out.println("  | WELCOME |");
    System.out.println("  -----------");
    System.out.println("  ^  ^  ^  ^  ^  ^ ");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\ ");
    System.out.println("<-S--A--S--2--2--2->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ / ");
    System.out.println("  v  v  v  v  v  v  ");
  }
}