import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Scanner;
////////
//// Lab09
//// Takes input for the number of slots in an array and inserts random integers in slots
	///Searches for specific value in array via binary search or linear search
//@author ssaini
//@version April 12, 2019
///

public class Lab09 {
	public static int[] generateArray(int n){ //Generates array for linear search
		int[] ourArray = new int[n];
		for(int i = 0; i < n; i ++){
			ourArray[i] = (int)(Math.random() * (n + 1));
		}
		return ourArray;
	}

	public static int[] generateOrderedArray(int n){ //Generates array for binary search
		int[] ourArray = generateArray(n);
		Arrays.sort(ourArray);
		return ourArray;
	}
	
	public static int findElement(int[] thisArray, int findThis){ //Linear search for specified element
		int indexOfElement = -1;
		for(int i = 0; i < thisArray.length; i ++){
			if (thisArray[i] == findThis){
				indexOfElement = i;
			}
		}
		return indexOfElement;
	}
	
	public static int findElementBin(int[] thisArray, int findThis){ //Binary search for specified element
		int indexOfElement = -1;
		int i = 0;
		int thisValue;
		int minRange = 0;
		int maxRange = thisArray.length;
		
		while(minRange <= maxRange){
			i = (maxRange + minRange) / 2;
			thisValue = thisArray[i];
			
			if (thisValue < findThis){
				if(minRange == i){
					break;
				}
				else{
					minRange = i;
					
				}
			}
			else if (thisValue > findThis){
				if(maxRange == i){
					break;		
				}
				else{
					maxRange = i;
				}
			}
			else{
				indexOfElement = i;
				break;
			}
		}
		return indexOfElement;
	}
	
	
	public static void main(String[] args) {
		Scanner myScanner;
		String junkWord = "";
		String operation = "";
		System.out.print("Do you want to do a linear search or a binary search? ");
		do{
			myScanner = new Scanner(System.in);
			junkWord = myScanner.next().toLowerCase();
			if (junkWord.equals("linear") || junkWord.equals("binary")){
				operation = junkWord;
			}
			else{
				System.out.print("Error, please enter \"linear\" or \"binary\": ");
			}
		}while(!(junkWord.equals("linear") || junkWord.equals("binary")));
		
		boolean good = false;
		int posInt = -1; 
		do{
			myScanner = new Scanner(System.in);
			System.out.print("Please enter the size of your array (positive integer): ");
			if (myScanner.hasNextInt()){
				posInt = myScanner.nextInt();
				if(posInt >= 0){
					good = true;
				}
			}
		} while(!good);			//Continues to ask for input until the user gives correct input
		System.out.println();
	
		good = false;
		int findThis = -1; 
		do{
			myScanner = new Scanner(System.in);
			System.out.print("Please enter the value you are trying to find (positive integer): ");
			if (myScanner.hasNextInt()){
				findThis = myScanner.nextInt();
				if(findThis >= 0){
					good = true;
				}
			}
		} while(!good);			//Continues to ask for input until the user gives correct input
		System.out.println();
		
		int[] theArray;
		int location;
		
		if(operation.equals("binary")){
			theArray = generateOrderedArray(posInt);
			location = findElementBin(theArray, findThis);
		}
		else{
			theArray = generateArray(posInt);
			location = findElement(theArray, findThis);
		}
		System.out.println("Here is your array: ");
		System.out.print("{ " + theArray[0] + " ");
		for (int i = 1; i < theArray.length; i ++){
			System.out.print(theArray[i]+ " ");
		}
		System.out.println("}");		
		System.out.println();
		
		System.out.println("The index of the value that you're looking for is: " + location);

	} //end of main method

} //end of class
